package nl.rubix.service.camelRestDsl;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;


public class CamelRestDslRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		restConfiguration()
			.component("jetty")
			.host("localhost")
			.port(9091)
			.dataFormatProperty("prettyPrint", "true")
			.bindingMode(RestBindingMode.json);
		

		
		rest("/restDsl/")
			.get("/getOperation/{id}").id("getOperationRest").to("direct:getOperation")
			.post("/postOperatoin").id("postOperationRest").type(OrderInputPojo.class).to("direct:postOperation")
		;
		
		from("direct:getOperation").routeId("getOperation")
			.log("body: ${header.id}")
			.processRef("orderInputProcessor")
			;
		
		from("direct:postOperation").routeId("postOperation")
		.log("body: ${body}")
		;
	}

	

}
