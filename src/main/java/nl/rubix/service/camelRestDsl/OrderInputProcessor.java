package nl.rubix.service.camelRestDsl;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class OrderInputProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		String id = exchange.getIn().getHeader("id").toString();
		
		OrderInputPojo output = new OrderInputPojo();
		output.setOrderID(id);
		output.setNaam("FietsbelOrder");
		output.setProduct("Fietsbel");
		exchange.getOut().setBody(output);
		
	}

}
